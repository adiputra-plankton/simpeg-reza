/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.17 : Database - l52_big
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`l52_big` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `l52_big`;

/*Table structure for table `kerja` */

DROP TABLE IF EXISTS `kerja`;

CREATE TABLE `kerja` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no` int(10) unsigned NOT NULL,
  `perusahaan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `periode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `kerja` */

insert  into `kerja`(`id`,`no`,`perusahaan`,`periode`,`posisi`) values (1,1,'4','2013','Junior'),(2,1,'1','2018','Senior'),(3,1,'2','2017','Midle'),(5,3,'1','asdfsdf','asdfsdf'),(6,4,'1','','');

/*Table structure for table `kursus` */

DROP TABLE IF EXISTS `kursus`;

CREATE TABLE `kursus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no` int(10) unsigned NOT NULL,
  `kursus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `kursus` */

insert  into `kursus`(`id`,`no`,`kursus`) values (1,1,'Bahasa Inggris'),(2,1,'Bahasa Indonesia'),(4,3,'asdfsdf'),(5,4,'');

/*Table structure for table `main` */

DROP TABLE IF EXISTS `main`;

CREATE TABLE `main` (
  `no` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perusahaan` int(10) unsigned NOT NULL,
  `nik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `npwp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tempatlahir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggallahir` date NOT NULL,
  `telponrumah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statuskaryawan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggalmasuk` date NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proyek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `masakerja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `golongandarah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `main` */

insert  into `main`(`no`,`nama`,`perusahaan`,`nik`,`npwp`,`alamat`,`tempatlahir`,`tanggallahir`,`telponrumah`,`status`,`statuskaryawan`,`tanggalmasuk`,`jabatan`,`proyek`,`masakerja`,`agama`,`gender`,`golongandarah`) values (1,'Bambang',3,'99881234567899','31398694','Depok, Jawa Barat','','0000-00-00','0218984759','Single','tetap','2018-08-22','Staff Ahli','Tangguh','4 Tahun 3 Bulan','Islam','laki-laki','A'),(3,'reza',1,'123123','12423','sdfsdf','sdfsdf','2019-01-08','134234','sdfsdfs','tetap','2019-01-01','','fsdf','sdfsdf','sdfsdf','laki-laki','A'),(4,'',1,'','','','','0000-00-00','','','tetap','0000-00-00','','','','','laki-laki','A');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2019_01_14_070543_create_main_table',1),('2019_01_14_070623_create_perusahaan_table',1),('2019_01_14_071130_create_status_table',1),('2019_01_14_071144_create_sim_table',1),('2019_01_14_071225_create_kursus_table',1),('2019_01_14_071240_create_kerja_table',1),('2019_01_14_071420_create_pendidikan_table',1),('2019_01_14_071931_create_studi_table',1),('2019_01_14_072649_create_mobile_table',1),('2019_01_14_075325_create_main_ref',1),('2019_01_14_075336_create_pendidikan_ref',1),('2019_01_14_080101_create_5table_ref',1);

/*Table structure for table `mobile` */

DROP TABLE IF EXISTS `mobile`;

CREATE TABLE `mobile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no` int(10) unsigned NOT NULL,
  `nomor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `mobile` */

insert  into `mobile`(`id`,`no`,`nomor`) values (1,1,'08123456'),(2,1,'0888334455'),(3,1,'08961234'),(5,3,'sdfsdfsdfdsf'),(6,4,'');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pendidikan` */

DROP TABLE IF EXISTS `pendidikan`;

CREATE TABLE `pendidikan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no` int(10) unsigned NOT NULL,
  `jenjang` int(10) unsigned NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `periode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bidangstudi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fakultas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pendidikan` */

insert  into `pendidikan`(`id`,`no`,`jenjang`,`nama`,`periode`,`bidangstudi`,`fakultas`) values (26,3,1,'asdfsdf','sdfsdfsdf','adfadfaf','sdfsdf'),(27,4,1,'','','',''),(28,1,1,'SDN 06','2000-2006','',''),(29,1,3,'SMA Swasta 60','2009-2012','IPA',''),(30,1,2,'SMP 123','2006-2009','',''),(31,1,1,'','','',''),(32,1,1,'','','',''),(33,1,1,'','','','');

/*Table structure for table `perusahaan` */

DROP TABLE IF EXISTS `perusahaan`;

CREATE TABLE `perusahaan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `perusahaan` */

insert  into `perusahaan`(`id`,`nama`) values (1,'MRI'),(2,'SPI'),(3,'UBS'),(4,'MII');

/*Table structure for table `sim` */

DROP TABLE IF EXISTS `sim`;

CREATE TABLE `sim` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no` int(10) DEFAULT NULL,
  `jenis` varchar(10) DEFAULT NULL,
  `nomorsim` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sim` */

/*Table structure for table `studi` */

DROP TABLE IF EXISTS `studi`;

CREATE TABLE `studi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenjang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `studi` */

insert  into `studi`(`id`,`jenjang`) values (1,'SD'),(2,'SMP'),(3,'SMA'),(4,'D1'),(5,'D2'),(6,'D3'),(7,'S1'),(8,'S2'),(9,'S3');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'anang','anangreza@gmail.com','$2y$10$ud3.dG.TcRFb6sQ5EeEo8uUqopFH.HjEgMBx1ppu6DZjrH/DBuJrW','BevFxyTjmUoqh2vj1HwgXTilWUdcw3aHfAi1vq2H0ig10K30NA7SslPNq4Yx','2019-01-17 01:32:19','2019-01-17 01:43:52'),(2,'Administrator','admin@mail.com','$2y$10$Wdvv6hs1U76AAkLa6AGDg.MTqFBr9aDwmkYEcLLPbJv3Ff/.f4ra6',NULL,'2019-01-19 17:16:31','2019-01-19 17:16:31');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
