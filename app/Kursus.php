<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kursus extends Model
{
    protected $table = 'kursus';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['no', 'kursus'];
}
