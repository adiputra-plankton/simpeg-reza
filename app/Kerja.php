<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kerja extends Model
{
    protected $table = 'kerja';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['no', 'perusahaan', 'periode', 'posisi'];
}
