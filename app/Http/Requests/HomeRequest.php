<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;


class HomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      switch($this->method())
  {
      case 'GET':
      case 'DELETE':
      {
          return [];
      }
      case 'POST':
      {
        return [
            'nama' => 'required',
            'perusahaanid' => 'required',
            'status' => 'required',
            'statuspgw' => 'required',
            'tanggalmasuk' => 'required|date_format:"d-m-Y"',
            'npwp' => 'required',
            'nik' => 'required',
            'jabatan' => 'required',
            'proyek' => 'required',
            'masakerja' => 'required',
            'alamat' => 'required',
            'telponrumah' => 'required',
            'gender' => 'required|date_format:"d-m-Y"',
            'tempatlahir' => 'required',
            'tanggallahir' => 'required|date_format:"d-m-Y"',
            'golongandarah' => 'required',
        ];
      }
      case 'PUT':
        return [
          'nama' => 'required',
          'perusahaanid' => 'required',
          'status' => 'required',
          'statuspgw' => 'required',
          'tanggalmasuk' => 'required|date_format:"d-m-Y"',
          'npwp' => 'required',
          'nik' => 'required',
          'jabatan' => 'required',
          'proyek' => 'required',
          'masakerja' => 'required',
          'alamat' => 'required',
          'telponrumah' => 'required',
          'gender' => 'required|date_format:"d-m-Y"',
          'tempatlahir' => 'required',
          'tanggallahir' => 'required|date_format:"d-m-Y"',
          'golongandarah' => 'required',
        ];
      case 'PATCH':
      default:break;
  }



    }

    public function messages()
    {
        return [
            'avatar.mimes' => 'Not a valid file type. Valid types include jpeg, bmp and png.'
        ];
    }
}
