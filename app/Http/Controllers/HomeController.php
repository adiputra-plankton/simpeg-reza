<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use DB;
use App\Main;
use App\Perusahaan;
use App\Sim;
//use App\Statuspgw;
use App\Kerja;
use App\Kursus;
use App\Pendidikan;
use App\Studi;
use App\Mobile;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perusahaans = Perusahaan::all();
        $studis = Studi::all();

        if(env('adminlte') == 'true') {
            return view('adminlte.home', compact('perusahaans', 'studis'));
        } else {
            return view('home', compact('perusahaans', 'studis'));
        }
    }

    public function edit(Request $request)
    {
        $perusahaans = Perusahaan::all();
        $studis = Studi::all();
        $pegawai = DB::table('main')
        ->where('no', $request->nip)
        ->first();

        $pendidikan = DB::table('pendidikan')
        ->where('no', $request->nip)
        ->get();

        $kursus = DB::table('kursus')
        ->where('no', $request->nip)
        ->get();

        $kerja = DB::table('kerja')
        ->where('no', $request->nip)
        ->get();

        $sim = DB::table('sim')
        ->where('no', $request->nip)
        ->get();

        $mobile = DB::table('mobile')
        ->where('no', $request->nip)
        ->get();

        $nip = $request->nip;

        if(env('adminlte') == 'true') {
            return view('adminlte.edit', compact('perusahaans', 'studis', 'pegawai', 'pendidikan', 'kursus', 'kerja', 'sim', 'mobile', 'nip'));
        } else {
            return view('edit', compact('perusahaans', 'studis', 'pegawai', 'pendidikan', 'kursus', 'kerja', 'sim', 'mobile', 'nip'));
        }
    }

    public function update(Request $request){
        //dd($request->all());
        $main = Main::find($request->nip);
        $main->nama                  = $request->nama;
        $main->perusahaan            = $request->perusahaanid;
        $main->status                = $request->status;
        $main->statuskaryawan        = $request->statuspgw;
        $main->tanggalmasuk          = $request->tanggalmasuk;
        $main->npwp                  = $request->npwp;
        $main->nik                   = $request->nik;
        $main->jabatan               = $request->jabatan;
        $main->proyek                = $request->proyek;
        $main->masakerja             = $request->masakerja;
        $main->alamat                = $request->alamat;
        $main->telponrumah           = $request->telponrumah;
        $main->agama                 = $request->agama;
        $main->gender                = $request->gender;
        $main->tempatlahir           = $request->tempatlahir;
        $main->tanggallahir          = $request->tanggallahir;
        $main->golongandarah         = $request->golongandarah;
        $main->save();

        if($request->datapendidikan == 1){
            $delpend = DB::table('pendidikan')->where('no', $request->nip)->delete();
            $cPendidikan = (count($request->jenjangpendidikan)-1);
            for ($i=0; $i < $cPendidikan; $i++) {
              if($request->namapendidikan[$i] != null)
                $pendidikan = Pendidikan::create([
                    'no'            => $request->nip,
                    'jenjang'       => $request->jenjangpendidikan[$i],
                    'nama'          => $request->namapendidikan[$i],
                    'periode'       => $request->periodependidikan[$i],
                    'bidangstudi'   => $request->bidangstudipendidikan[$i],
                    'fakultas'      => $request->fakultaspendidikan[$i]
                ]);
            }
        }

        if($request->datakursus == 1){
            $delkursus =  DB::table('kursus')->where('no', $request->nip)->delete();
            $cKursus = (count($request->namakursus)-1);
            for ($j=0; $j < $cKursus; $j++) {
              if($request->namakursus[$j] != null)
                $kursus = Kursus::create([
                    'no'            => $request->nip,
                    'kursus'        => $request->namakursus[$j]
                ]);
            }
        }

        if($request->datakerja == 1){
            $delkerja =  DB::table('kerja')->where('no', $request->nip)->delete();
            $cKerja = (count($request->perusahaan)-1);
            for ($k=0; $k < $cKerja; $k++) {
              if($request->perusahaan[$k] != null)
                $kerja = Kerja::create([
                    'no'            => $request->nip,
                    'perusahaan'    => $request->perusahaan[$k],
                    'periode'       => $request->periodepekerjaan[$k],
                    'posisi'        => $request->posisi[$k]
                ]);
            }
        }

        if($request->datasim == 1){
            $delsim =  DB::table('sim')->where('no', $request->nip)->delete();
            $cSim = (count($request->jenissim)-1);
            for ($l=0; $l < $cSim; $l++) {
              if($request->jenissim[$l] != null)
                $sim = Sim::create([
                    'no'            => $request->nip,
                    'jenis'         => $request->jenissim[$l],
                    'nomorsim'      => $request->nosim[$l]
                ]);
            }
        }

        if($request->datamobile == 1){
            $delmobile =  DB::table('mobile')->where('no', $request->nip)->delete();
            $cMobile = (count($request->nomormobile)-1);
            for ($m=0; $m < $cMobile; $m++) {
              if($request->nomormobile[$m] != null)
                $mobile = Mobile::create([
                    'no'            => $request->nip,
                    'nomor'         => $request->nomormobile[$m]
                ]);
            }
        }

        return redirect('/view');

    }

    // public function show(Request $request)
    // {
    //     $perusahaans = Perusahaan::all();
    //     $studis = Studi::all();
    //     $pegawai = DB::table('main')
    //     ->where('no', $request->perusahaans)
    //     ->first();

    //     $pendidikan = DB::table('pendidikan')
    //     ->where('no', $request->perusahaans)
    //     ->get();

    //     $kursus = DB::table('kursus')
    //     ->where('no', $request->perusahaans)
    //     ->get();

    //     $kerja = DB::table('kerja')
    //     ->where('no', $request->perusahaans)
    //     ->get();

    //     $sim = DB::table('sim')
    //     ->where('no', $request->perusahaans)
    //     ->get();

    //     $mobile = DB::table('mobile')
    //     ->where('no', $request->perusahaans)
    //     ->get();

    //     $perusahaan = $request->perusahaans;

    //     return view('list', compact('perusahaans', 'studis', 'pegawai', 'pendidikan', 'kursus', 'kerja', 'sim', 'mobile', 'nip'));
    // }

    public function listData(Request $request){
        //dd($request->all());
        $main = Main::find($request->perusahaans);
        $main->nama                  = $request->nama;
        $main->perusahaan            = $request->perusahaanid;
        $main->status                = $request->status;
        $main->statuskaryawan        = $request->statuspgw;
        $main->tanggalmasuk          = $request->tanggalmasuk;
        $main->npwp                  = $request->npwp;
        $main->nik                   = $request->nik;
        $main->jabatan               = $request->jabatan;
        $main->proyek                = $request->proyek;
        $main->masakerja             = $request->masakerja;
        $main->alamat                = $request->alamat;
        $main->telponrumah           = $request->telponrumah;
        $main->agama                 = $request->agama;
        $main->gender                = $request->gender;
        $main->tempatlahir           = $request->tempatlahir;
        $main->tanggallahir          = $request->tanggallahir;
        $main->golongandarah         = $request->golongandarah;
        $main->save();

        if($request->datapendidikan == 1){
            $delpend = DB::table('pendidikan')->where('no', $request->nip)->delete();
            $cPendidikan = (count($request->jenjangpendidikan)-1);
            for ($i=0; $i < $cPendidikan; $i++) {
              if($request->namapendidikan[$i] != null)
                $pendidikan = Pendidikan::create([
                    'no'            => $request->nip,
                    'jenjang'       => $request->jenjangpendidikan[$i],
                    'nama'          => $request->namapendidikan[$i],
                    'periode'       => $request->periodependidikan[$i],
                    'bidangstudi'   => $request->bidangstudipendidikan[$i],
                    'fakultas'      => $request->fakultaspendidikan[$i]
                ]);
            }
        }

        if($request->datakursus == 1){
            $delkursus =  DB::table('kursus')->where('no', $request->nip)->delete();
            $cKursus = (count($request->namakursus)-1);
            for ($j=0; $j < $cKursus; $j++) {
              if($request->namakursus[$j] != null)
                $kursus = Kursus::create([
                    'no'            => $request->nip,
                    'kursus'        => $request->namakursus[$j]
                ]);
            }
        }

        if($request->datakerja == 1){
            $delkerja =  DB::table('kerja')->where('no', $request->nip)->delete();
            $cKerja = (count($request->perusahaan)-1);
            for ($k=0; $k < $cKerja; $k++) {
              if($request->perusahaan[$k] != null)
                $kerja = Kerja::create([
                    'no'            => $request->nip,
                    'perusahaan'    => $request->perusahaan[$k],
                    'periode'       => $request->periodepekerjaan[$k],
                    'posisi'        => $request->posisi[$k]
                ]);
            }
        }

        if($request->datasim == 1){
            $delsim =  DB::table('sim')->where('no', $request->nip)->delete();
            $cSim = (count($request->jenissim)-1);
            for ($l=0; $l < $cSim; $l++) {
              if($request->jenissim[$l] != null)
                $sim = Sim::create([
                    'no'            => $request->nip,
                    'jenis'         => $request->jenissim[$l],
                    'nomorsim'      => $request->nosim[$l]
                ]);
            }
        }

        if($request->datamobile == 1){
            $delmobile =  DB::table('mobile')->where('no', $request->nip)->delete();
            $cMobile = (count($request->nomormobile)-1);
            for ($m=0; $m < $cMobile; $m++) {
              if($request->nomormobile[$m] != null)
                $mobile = Mobile::create([
                    'no'            => $request->nip,
                    'nomor'         => $request->nomormobile[$m]
                ]);
            }
        }

        return redirect('/view');

    }

    public function store(Request $request)
    {
        $pegawai = Main::create([
            'nama'                  => $request->nama,
            'perusahaan'            => $request->perusahaanid,
            'status'                => $request->status,
            'statuskaryawan'        => $request->statuspgw,
            'tanggalmasuk'          => $request->tanggalmasuk,
            'npwp'                  => $request->npwp,
            'nik'                   => $request->nik,
            'jabatan'               => $request->jabatan,
            'proyek'                => $request->proyek,
            'masakerja'             => $request->masakerja,
            'alamat'                => $request->alamat,
            'telponrumah'           => $request->telponrumah,
            'agama'                 => $request->agama,
            'gender'                => $request->gender,
            'tempatlahir'           => $request->tempatlahir,
            'tanggallahir'          => $request->tanggallahir,
            'golongandarah'         => $request->golongandarah
        ]);

        $lastInsertedId = $pegawai->no;
        $cPendidikan = (count($request->jenjangpendidikan)-1);
        for ($i=0; $i < $cPendidikan; $i++) {
            $pendidikan = Pendidikan::create([
                'no'            => $lastInsertedId,
                'jenjang'       => $request->jenjangpendidikan[$i],
                'nama'          => $request->namapendidikan[$i],
                'periode'       => $request->periodependidikan[$i],
                'bidangstudi'   => $request->bidangstudipendidikan[$i],
                'fakultas'      => $request->fakultaspendidikan[$i]
            ]);
        }

        $cKursus = (count($request->namakursus)-1);
        for ($j=0; $j < $cKursus; $j++) {
            $kursus = Kursus::create([
                'no'            => $lastInsertedId,
                'kursus'        => $request->namakursus[$j]
            ]);
        }

        $cKerja = (count($request->perusahaan)-1);
        for ($k=0; $k < $cKerja; $k++) {
            $kerja = Kerja::create([
                'no'            => $lastInsertedId,
                'perusahaan'    => $request->perusahaan[$k],
                'periode'       => $request->periodepekerjaan[$k],
                'posisi'        => $request->posisi[$k]
            ]);
        }

        $cSim = (count($request->jenissim)-1);
        for ($l=0; $l < $cSim; $l++) {
            $sim = Sim::create([
                'no'            => $lastInsertedId,
                'jenis'         => $request->jenissim[$l],
                'nomorsim'      => $request->nosim[$l]
            ]);
        }

        $cMobile = (count($request->nomormobile)-1);
        for ($m=0; $m < $cMobile; $m++) {
            $mobile = Mobile::create([
                'no'            => $lastInsertedId,
                'nomor'         => $request->nomormobile[$m]
            ]);
        }

        Session::flash('message', 'Input Data Pegawai: ' . $request->nip . ' Sukses');
        return redirect()->back();
    }

    public function delete($id){
        $delmain = DB::table('main')->where('no', $id)->delete();
        $delpend = DB::table('pendidikan')->where('no', $id)->delete();
        $delkursus =  DB::table('kursus')->where('no', $id)->delete();
        $delkerja =  DB::table('kerja')->where('no', $id)->delete();
        $delsim =  DB::table('sim')->where('no', $id)->delete();
        $delmobile =  DB::table('mobile')->where('no', $id)->delete();

        return redirect('/view');
    }
}
