<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Perusahaan;
use DB;
use PDF;

class PerusahaanController extends Controller
{
    public function index()
	{
        $perusahaans = Perusahaan::all();

        if(env('adminlte') == 'true') {
            return view('adminlte.perusahaan.index', compact('perusahaans'));
        } else {
            return view('perusahaan.index', compact('perusahaans'));
        }
	}

    public function filter(Request $request)
    {
        $data_filter = $this->get_filter_data($request);
        $data_filter['sessionQuery'] = http_build_query(\Session::get('requestData'));

        if(env('adminlte') == 'true') {
            return view('adminlte.perusahaan.filter',$data_filter);
        } else {
            return view('perusahaan.filter',$data_filter);
        }
    }

    public function download_pdf(Request $request)
    {
        $data_filter = $this->get_filter_data($request);

        // if($data_filter['withMultipleData']) {
            if(env('adminlte') == 'true') {
                $view = 'adminlte.perusahaan.download_pdf_multiple_data';
            } else {
                $view = 'perusahaan.download_pdf_multiple_data';
            }
            // return view('perusahaan.download_pdf_multiple_data',$data_filter);
            // } else {
        //     $view = 'perusahaan.download_pdf';
        //     // return view('perusahaan.download_pdf',$data_filter);
        // }
                
        // return view($view,$data_filter);
        $pdf = PDF::loadView($view, $data_filter)->setPaper('a4');
        return $pdf->stream();
    }

    private function get_filter_data($request) 
    {
        $perusahaanId = $request->get('perusahaanid');
        $selectAll = $request->get('selectall');

        $requestData = $request->all();
        \Session::set('requestData', $requestData);
        $filters = array_keys($request->all());
        
        if($request->namaperusahaan == '1') {
            $excludeFilter = [
                '_token',
                'perusahaanid',
                'selectall',
                'nama',
                'namaperusahaan',
                'pendidikan',
                'kursus',
                'pekerjaan',
                'sim',
                'mobile'
            ];
        } else {
            $excludeFilter = [
                '_token',
                'perusahaanid',
                'selectall',
                'pendidikan',
                'kursus',
                'pekerjaan',
                'sim',
                'mobile'
            ];
        }


        $arrayMainSelect = array_diff($filters, $excludeFilter);
        $mainSelect = implode(',', $arrayMainSelect);

        if($request->namaperusahaan == '1') {
            $excludeFilter = [
                '_token',
                'perusahaanid',
                'selectall',
                'pendidikan',
                'kursus',
                'pekerjaan',
                'sim',
                'mobile'
            ];

            $arrayMainSelect = array_diff($filters, $excludeFilter);
        }
        
        if($perusahaanId > 0) {
            $perusahaan = DB::table('perusahaan')->where('id', $perusahaanId)->first();
            
            if($request->namaperusahaan == '1') {
                if($mainSelect != "") {
                    $employees = DB::table('main')
                        ->leftJoin('perusahaan', 'perusahaan.id', '=', 'main.perusahaan')
                        ->where('perusahaan', $perusahaanId)
                        ->get([
                            DB::raw(
                                'no' . ', ' . $mainSelect . ', main.nama as nama, perusahaan.nama as namaperusahaan'
                            )
                        ]);
                } else {
                    $employees = DB::table('main')
                        ->leftJoin('perusahaan', 'perusahaan.id', '=', 'main.perusahaan')
                        ->where('perusahaan', $perusahaanId)
                        ->get([
                            DB::raw(
                                'no' . ', main.nama as nama, perusahaan.nama as namaperusahaan'
                            )
                        ]);
                }
            } else {
                if($mainSelect != "") {
                    $employees = DB::table('main')
                        ->where('perusahaan', $perusahaanId)
                        ->get([
                            DB::raw(
                                'no' . ', ' . $mainSelect
                            )
                        ]);
                } else {
                    $employees = DB::table('main')
                        ->where('perusahaan', $perusahaanId)
                        ->get([
                            DB::raw(
                                'no'
                            )
                        ]);
                }
            }

        } else {
            $perusahaan = null;

            if($request->namaperusahaan == '1') {
                if($mainSelect != "") {
                    $employees = DB::table('main')
                        ->leftJoin('perusahaan', 'perusahaan.id', '=', 'main.perusahaan')
                        ->get([
                            DB::raw(
                                'no' . ', ' . $mainSelect . ', main.nama as nama, perusahaan.nama as namaperusahaan'
                            )
                        ]);
                } else {
                    $employees = DB::table('main')
                        ->leftJoin('perusahaan', 'perusahaan.id', '=', 'main.perusahaan')
                        ->get([
                            DB::raw(
                                'no, main.nama as nama, perusahaan.nama as namaperusahaan'
                            )
                        ]);
                }
            } else {
                if($mainSelect != "") {
                    $employees = DB::table('main')
                        ->get([
                            DB::raw('no' . ', ' . $mainSelect)
                        ]);
                } else {
                    $employees = DB::table('main')
                        ->get([
                            DB::raw('no')
                        ]);
                }
            }
        }

        
        $employeesData = [];
        $withMultipleData = false;

        if($selectAll && $selectAll == '1') {
            $withMultipleData = true;
            foreach($employees as $index => $employee) {
                
                $employeesData[$index]['employee'] = $employee;

                $pendidikan = DB::table('pendidikan')
                    ->leftJoin('studi', 'studi.id', '=', 'pendidikan.jenjang')
                    ->where('no', $employee->no)
                    ->get([
                        'studi.jenjang as jenjang_name',
                        'pendidikan.nama',
                        'pendidikan.periode',
                        'pendidikan.bidangstudi',
                        'pendidikan.fakultas'
                    ]);
                $employeesData[$index]['employee_pendidikan'] = $pendidikan;
                
                $kursus = DB::table('kursus')
                    ->where('no', $employee->no)
                    ->get();
                $employeesData[$index]['employee_kursus'] = $kursus;
            
                $pekerjaan = DB::table('kerja')
                    ->where('no', $employee->no)
                    ->get([
                        'kerja.perusahaan as perusahaan_name',
                        'kerja.periode',
                        'kerja.posisi'
                    ]);
                $employeesData[$index]['employee_pekerjaan'] = $pekerjaan;
            
                $sim = DB::table('sim')
                    ->where('no', $employee->no)
                    ->get();
                $employeesData[$index]['employee_sim'] = $sim;

                $mobile = DB::table('mobile')
                    ->where('no', $employee->no)
                    ->get();
                $employeesData[$index]['employee_mobile'] = $mobile;
            }
            
        } else {

            $filterPendidikan = $request->get('pendidikan');
            $filterKursus = $request->get('kursus');
            $filterPekerjaan = $request->get('pekerjaan');
            $filterSIM = $request->get('sim');
            $filterMobile = $request->get('mobile');
            if($filterPekerjaan || $filterKursus || $filterPekerjaan || $filterSIM || $filterMobile) {
                $withMultipleData = true;
            }

            foreach($employees as $index => $employee) {
                
                $employeesData[$index]['employee'] = $employee;

                if($filterPendidikan && $filterPendidikan == '1') {
                    $pendidikan = DB::table('pendidikan')
                        ->leftJoin('studi', 'studi.id', '=', 'pendidikan.jenjang')
                        ->where('no', $employee->no)
                        ->get([
                            'studi.jenjang as jenjang_name',
                            'pendidikan.nama',
                            'pendidikan.periode',
                            'pendidikan.bidangstudi',
                            'pendidikan.fakultas'
                        ]);
                    
                    $employeesData[$index]['employee_pendidikan'] = $pendidikan;
                }
    
                if($filterKursus && $filterKursus == '1') {
                    $kursus = DB::table('kursus')
                        ->where('no', $employee->no)
                        ->get();
                    
                    $employeesData[$index]['employee_kursus'] = $kursus;
                }

                if($filterPekerjaan && $filterPekerjaan == '1') {
                    $pekerjaan = DB::table('kerja')
                        ->where('no', $employee->no)
                        ->get([
                            'kerja.perusahaan as perusahaan_name',
                            'kerja.periode',
                            'kerja.posisi'
                        ]);
                    
                    $employeesData[$index]['employee_pekerjaan'] = $pekerjaan;
                }

                if($filterSIM && $filterSIM == '1') {
                    $sim = DB::table('sim')
                        ->where('no', $employee->no)
                        ->get();
                    
                    $employeesData[$index]['employee_sim'] = $sim;
                }

                if($filterMobile && $filterMobile == '1') {
                    $mobile = DB::table('mobile')
                        ->where('no', $employee->no)
                        ->get();
                    
                    $employeesData[$index]['employee_mobile'] = $mobile;
                }
            }
            
        }

        return [
            'employeesData' => $employeesData,
            'filters' => $filters,
            'withMultipleData' => $withMultipleData,
            'perusahaan' => $perusahaan,
            'requestData' => $requestData,
            'arrayMainSelect' => $arrayMainSelect
        ];
    }
}