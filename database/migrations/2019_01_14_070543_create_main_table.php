<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main', function (Blueprint $table) {
            $table->increments('no');
            $table->string('nama');
            $table->integer('perusahaan')->unsigned();
            $table->string('nik');
            $table->string('npwp');
            $table->string('alamat');
            $table->string('tempatlahir');
            $table->date('tanggallahir');
            $table->string('telponrumah');
            $table->string('status');
            $table->integer('statuskaryawan')->unsigned();
            $table->date('tanggalmasuk');
            $table->string('jabatan');
            $table->string('proyek');
            $table->string('masakerja');
            $table->string('agama');
            $table->string('gender');
            $table->string('golongandarah');
//            $table->primary('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
