<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainRef extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('main', function (Blueprint $table) {
//            $table->foreign('statuskaryawan')->references('id')->on('statuspgw');
//        });
        
        Schema::table('main', function (Blueprint $table) {
            $table->foreign('perusahaan')->references('id')->on('perusahaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
