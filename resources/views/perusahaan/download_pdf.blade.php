<style>
    table {
    border-collapse: collapse;
    border: 1px solid red;
    margin-bottom: 1em;
    width: auto;
}

table tr {
    page-break-inside: avoid;
}

table thead tr td {
    background-color: #F0F0F0;
    border: 1px solid #DDDDDD;
    min-width: 0.6em;
    padding: 5px;
    text-align: left;
    vertical-align: top;
    font-weight: bold;
}

table tbody tr td {
    border: 1px solid #DDDDDD;
    min-width: 0.6em;
    padding: 5px;
    vertical-align: top;
}

tbody tr.even td {
    background-color: transparent;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Pegawai Perusahaan {{ $perusahaan->nama }}</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table border='1' id="report" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    
                                    <?php foreach($arrayMainSelect as $select): ?>
                                        <td><?php echo strtoupper($select); ?></td>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($employeesData as $index => $employeeData): ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <?php foreach($arrayMainSelect as $select): ?>
                                            <?php if( isset($employeesData[$index]['employee']->$select) ): ?>
                                                <td><?php echo strtoupper($employeesData[$index]['employee']->$select); ?></td>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                        <?php $no++; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <div>
                </div>
            </div>
        </div>
    </div>
</div>