@extends('layouts.app')

@section('content')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/select2/dist/css/select2.min.css') }}">
<script src="{{ asset('vendor/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(function () {
		//Initialize Select2 Elements
        $('.select2').select2();

        $("#selectall").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

    });
	</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Pegawai</div>
                <div class="panel-body">
                    <table id="report" class="table table-striped table-bordered" style="width:100%">

                    </table>
                    <hr>

                    <form action="{{ url('/edit') }}" method="post">
                        {{ csrf_field() }}
                            <div class="form-group">
                                    <label for="nip"> Pegawai &nbsp; </label>
                                    <select class="form-control{{ $errors->has('nip') ? ' is-invalid' : '' }} select2" name="nip" id="nip" style="width: 25%;" required>
                                      <option value="" readonly>Pilih Pegawai</option>
                                        @foreach($mains as $p)
                                        <option value="{{ $p->no }}">{{ $p->nama }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('nip'))
                                        <span class="invalid-feedback" style="color: red">
                                            <strong>{{ $errors->first('nip') }}</strong>
                                        </span>
                                    @endif
                            </div>

                        <div class="row">
                                <div class="col-md-12">
                                        <div class="checkbox">
                                                <label><input type="checkbox" name="selectall" id="selectall" value="1"><strong>Check All</strong></label>
                                        </div>
                                </div>

                            <div class="col-md-3">
                                <div class="checkbox">
                                    <label><input type="checkbox" class="pilih" name="nama" id="nama" value="1">Nama</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                    <div class="checkbox">
                                            <label><input type="checkbox" class="pilih" name="perusahaanid" id="perusahaanid" value="1">Perusahaan</label>
                                        </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox">
                                        <label><input type="checkbox" class="pilih" name="nik" id="nik" value="1">NIK</label>
                            </div>
                            </div>
                            <div class="col-md-3">
                                    <div class="checkbox">
                                            <label><input type="checkbox" class="pilih" name="npwp" id="npwp" value="1">NPWP</label>
                                </div>
                                </div>
                        </div>

                        <div class="row">
                                <div class="col-md-3">
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="pilih" name="alamat" id="alamat" value="1">Alamat</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="tempatlahir" id="tempatlahir" value="1">Tempat Lahir</label>
                                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox">
                                            <label><input type="checkbox" class="pilih" name="tanggallahir" id="tanggallahir" value="1">Tanggal Lahir</label>
                                </div>
                                </div>
                                <div class="col-md-3">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="telponrumah" id="telponrumah" value="1">Telepon Rumah</label>
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                                <div class="col-md-3">
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="pilih" name="status" id="status" value="1">Status</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="statuskaryawan" id="statuskaryawan" value="1">Status Karyawan</label>
                                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox">
                                            <label><input type="checkbox" class="pilih" name="tanggalmasuk" id="tanggalmasuk" value="1">Tanggal Masuk</label>
                                </div>
                                </div>
                                <div class="col-md-3">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="jabatan" id="jabatan" value="1">Jabatan</label>
                                    </div>
                                </div>

                        </div>

                        <div class="row">
                                <div class="col-md-3">
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="pilih" name="proyek" id="proyek" value="1">Proyek</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="masakerja" id="masakerja" value="1">Masa Kerja</label>
                                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox">
                                            <label><input type="checkbox" class="pilih" name="agama" id="agama" value="1">Agama</label>
                                </div>
                                </div>
                                <div class="col-md-3">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="gender" id="gender" value="1">Gender</label>
                                    </div>
                                </div>

                        </div>
                        <hr>

                        <div class="row">
                                <div class="col-md-2">
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="pilih" name="pendidikan" id="pendidikan" value="1">Pendidikan</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="kursus" id="kursus" value="1">Kursus</label>
                                            </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox">
                                            <label><input type="checkbox" class="pilih" name="pekerjaan" id="pekerjaan" value="1">Pekerjaan</label>
                                </div>
                                </div>
                                <div class="col-md-2">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="sim" id="sim" value="1">SIM</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                        <div class="checkbox">
                                                <label><input type="checkbox" class="pilih" name="mobile" id="mobile" value="1">Mobile</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                        &nbsp;
                                </div>

                        </div>
                        <hr>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" id="btnsubmit">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
