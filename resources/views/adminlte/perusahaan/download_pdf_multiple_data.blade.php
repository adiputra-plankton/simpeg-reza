<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
<style>

.page-break {
    page-break-after: always;
}

/* DivTable.com */
.divTable{
	display: table;
	width: 100%;
}
.divTableRow {
	display: table-row;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
}
.divTableCell, .divTableHead {
	border: 1px solid #999999;
	display: table-cell;
	padding: 3px 10px;
}
.divTableFoot {
	background-color: #EEE;
	display: table-footer-group;
	font-weight: bold;
}
.divTableBody {
	display: table-row-group;
}

</style>
</head>
<body>
    @if($perusahaan)
        <div class="panel-heading" align="center">Data Pegawai Perusahaan {{ $perusahaan->nama }}</div>
    @endif
    
    <div class="divTable" style="width: 100%;border: 1px solid #000;">
        <div class="divTableHeading">
            <div class="divTableRow">
                <div class="divTableCell">No</div>
                <div class="divTableCell">Data</div>
            </div>
        </div>
        <div class="divTableBody">
            <?php $no = 1; ?>
            <?php foreach($employeesData as $index => $employeeData): ?>
                <div class="divTableRow">
                    <div class="divTableCell"><?php echo $no; ?></div>
                    <div class="divTableCell">
                        <span>Data Pribadi</span>
                        <div class="divTable" style="width: 100%;border: 1px solid #000;">
                            <div class="divTableBody">
                                <?php $cols = array_chunk($arrayMainSelect, ceil(count($arrayMainSelect)/8)); ?>
                                <?php foreach($cols as $item): ?>
                                    <div class="divTableRow">
                                        <?php foreach($item as $select): ?>
                                            <?php if( isset($employeesData[$index]['employee']->$select) ): ?>
                                                <div class="divTableCell"><b><?php echo strtoupper($select); ?></b>: <?php echo strtoupper($employeesData[$index]['employee']->$select); ?></div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
            </div>
                                <?php endforeach; ?>
</div>
</div>

                        <?php if( array_key_exists('pendidikan', $requestData) && $requestData['pendidikan'] == "1" ): ?>
                            <?php if( count($employeesData[$index]['employee_pendidikan']) > 0 ): ?>
                                <span>Data Pendidikan</span>
                                <div class="divTable" style="width: 100%; border: 1px solid #000;">
                                    <div class="divTableHeading">
                                        <div class="divTableRow">
                                            <div class="divTableCell">No</div>
                                            <div class="divTableCell">Jenjang</div>
                                            <div class="divTableCell">Nama</div>
                                            <div class="divTableCell">Periode</div>
                                            <div class="divTableCell">Bidang Studi</div>
                                            <div class="divTableCell">Fakultas</div>
            </div>
        </div>
                                    <div class="divTableBody">
                                        <?php foreach($employeesData[$index]['employee_pendidikan'] as $key => $item): ?>
                                            <div class="divTableRow">
                                                <div class="divTableCell"><?php echo ($key + 1); ?></div>
                                                <div class="divTableCell"><?php echo $item->jenjang_name; ?></div>
                                                <div class="divTableCell"><?php echo $item->nama; ?></div>
                                                <div class="divTableCell"><?php echo $item->periode; ?></div>
                                                <div class="divTableCell"><?php echo $item->bidangstudi; ?></div>
                                                <div class="divTableCell"><?php echo $item->fakultas; ?></div>
            </div>
                                        <?php endforeach; ?>
</div>
</div>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <?php if( array_key_exists('kursus', $requestData) && $requestData['kursus'] == "1" ): ?>
                            <?php if( count($employeesData[$index]['employee_kursus']) > 0 ): ?>
                                <span>Data Kursus</span>
                                <div class="divTable" style="width: 100%;border: 1px solid #000;">
                                    <div class="divTableHeading">
                                        <div class="divTableRow">
                                            <div class="divTableCell">No</div>
                                            <div class="divTableCell">Nama Kursus</div>
            </div>
        </div>
                                    <div class="divTableBody">
                                        <?php foreach($employeesData[$index]['employee_kursus'] as $key => $item): ?>
                                            <div class="divTableRow">
                                                <div class="divTableCell"><?php echo ($key + 1); ?></div>
                                                <div class="divTableCell"><?php echo $item->kursus; ?></div>
            </div>
                                        <?php endforeach; ?>
</div>
</div>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if( array_key_exists('pekerjaan', $requestData) && $requestData['pekerjaan'] == "1" ): ?>
                            <?php if( count($employeesData[$index]['employee_pekerjaan']) > 0 ): ?>
                                <span>Data Pekerjaan</span>
                                <div class="divTable" style="width: 100%;border: 1px solid #000;">
                                    <div class="divTableHeading">
                                        <div class="divTableRow">
                                            <div class="divTableCell">No</div>
                                            <div class="divTableCell">Nama Perusahaan</div>
                                            <div class="divTableCell">Periode</div>
                                            <div class="divTableCell">Posisi</div>
            </div>
        </div>
                                    <div class="divTableBody">
                                        <?php foreach($employeesData[$index]['employee_pekerjaan'] as $key => $item): ?>
                                            <div class="divTableRow">
                                                <div class="divTableCell"><?php echo ($key + 1); ?></div>
                                                <div class="divTableCell"><?php echo $item->perusahaan_name; ?></div>
                                                <div class="divTableCell"><?php echo $item->periode; ?></div>
                                                <div class="divTableCell"><?php echo $item->posisi; ?></div>
            </div>
                                        <?php endforeach; ?>
</div>
</div>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <?php if( array_key_exists('sim', $requestData) && $requestData['sim'] == "1" ): ?>
                            <?php if( count($employeesData[$index]['employee_sim']) > 0 ): ?>
                                <span>Data SIM</span>
                                <div class="divTable" style="width: 100%;border: 1px solid #000;">
                                    <div class="divTableHeading">
                                        <div class="divTableRow">
                                            <div class="divTableCell">No</div>
                                            <div class="divTableCell">Jenis SIM</div>
                                            <div class="divTableCell">Nomor SIM</div>
            </div>
        </div>
                                    <div class="divTableBody">
                                        <?php foreach($employeesData[$index]['employee_sim'] as $key => $item): ?>
                                            <div class="divTableRow">
                                                <div class="divTableCell"><?php echo ($key + 1); ?></div>
                                                <div class="divTableCell"><?php echo $item->jenis; ?></div>
                                                <div class="divTableCell"><?php echo $item->nomorsim; ?></div>
            </div>
                                        <?php endforeach; ?>
</div>
</div>
                            <?php endif; ?>
                        <?php endif; ?>

                        
                        <?php if( array_key_exists('mobile', $requestData) && $requestData['mobile'] == "1" ): ?>
                        <?php if( count($employeesData[$index]['employee_mobile']) > 0 ): ?>
                                <span>Data Mobile</span>
                                <div class="divTable" style="width: 100%;border: 1px solid #000;">
                                    <div class="divTableHeading">
                                        <div class="divTableRow">
                                            <div class="divTableCell">No</div>
                                            <div class="divTableCell">Nomor</div>
            </div>
        </div>
                                    <div class="divTableBody">
                                        <?php foreach($employeesData[$index]['employee_mobile'] as $key => $item): ?>
                                            <div class="divTableRow">
                                                <div class="divTableCell"><?php echo ($key + 1); ?></div>
                                                <div class="divTableCell"><?php echo $item->nomor; ?></div>
            </div>
                                        <?php endforeach; ?>
</div>
</div>
                            <?php endif; ?>
                        <?php endif; ?>

                    </div>

                    <?php $no++; ?>
            </div>
            <?php endforeach; ?>
</div>
</div>
</body>
</html>