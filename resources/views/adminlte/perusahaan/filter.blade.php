@extends('layouts.adminlte')

@section('content')
<section class="content-header">
      <h1>
        Data Pegawai
        <small>Filter data pegawai berdasarkan perusahaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Pegawai</li>
      </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                @if($perusahaan)
                    <div class="panel-heading">Data Pegawai Perusahaan {{ $perusahaan->nama }}</div>
                @endif
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="report" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <?php foreach($arrayMainSelect as $select): ?>
                                        <td><?php echo strtoupper($select); ?></td>
                                    <?php endforeach; ?>
                                    
                                    <?php if( array_key_exists('pendidikan', $requestData) && $requestData['pendidikan'] == "1" ): ?>
                                        <td>PENDIDIKAN</td>
                                    <?php endif; ?>

                                    <?php if( array_key_exists('kursus', $requestData) && $requestData['kursus'] == "1" ): ?>
                                        <td>KURSUS</td>
                                    <?php endif; ?>

                                    <?php if( array_key_exists('pekerjaan', $requestData) && $requestData['pekerjaan'] == "1" ): ?>
                                        <td>PEKERJAAN</td>
                                    <?php endif; ?>
                                    
                                    <?php if( array_key_exists('sim', $requestData) && $requestData['sim'] == "1" ): ?>
                                        <td>SIM</td>
                                    <?php endif; ?>

                                    <?php if( array_key_exists('mobile', $requestData) && $requestData['mobile'] == "1" ): ?>
                                        <td>MOBILE</td>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($employeesData as $index => $employeeData): ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <?php foreach($arrayMainSelect as $select): ?>
                                            <?php if( isset($employeesData[$index]['employee']->$select) ): ?>
                                                <td><?php echo strtoupper($employeesData[$index]['employee']->$select); ?></td>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                        <?php if( array_key_exists('pendidikan', $requestData) && $requestData['pendidikan'] == "1" ): ?>
                                            <?php if( count($employeesData[$index]['employee_pendidikan'] > 0) ): ?>
                                                <td>
                                                    <?php foreach($employeesData[$index]['employee_pendidikan'] as $key => $item): ?>
                                                        <?php echo '(' .($key + 1). ') Jenjang: ' . $item->jenjang_name . '<br />'; ?>
                                                        <?php echo 'Nama: ' . $item->nama . '<br />'; ?>
                                                        <?php echo 'Periode: ' . $item->periode . '<br />'; ?>
                                                        <?php echo 'Bidang Studi: ' . $item->bidangstudi . '<br />'; ?>
                                                        <?php echo 'Fakultas: ' . $item->fakultas . '<br />'; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if( array_key_exists('kursus', $requestData) && $requestData['kursus'] == "1" ): ?>
                                            <?php if( count($employeesData[$index]['employee_kursus'] > 0) ): ?>
                                                <td>
                                                    <?php foreach($employeesData[$index]['employee_kursus'] as $key => $item): ?>
                                                        <?php if($item->kursus != ""): ?>
                                                            <?php echo '(' .($key + 1). ') Nama Kursus: ' . $item->kursus . '<br />'; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if( array_key_exists('pekerjaan', $requestData) && $requestData['pekerjaan'] == "1" ): ?>
                                            <?php if( count($employeesData[$index]['employee_pekerjaan'] > 0) ): ?>
                                                <td>
                                                    <?php foreach($employeesData[$index]['employee_pekerjaan'] as $key => $item): ?>
                                                        <?php echo '(' .($key + 1). ') Nama Perusahaan: ' . $item->perusahaan_name . '<br />'; ?>
                                                        <?php echo 'Periode: ' . $item->periode . '<br />'; ?>
                                                        <?php echo 'Posisi: ' . $item->posisi . '<br />'; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if( array_key_exists('sim', $requestData) && $requestData['sim'] == "1" ): ?>
                                            <?php if( count($employeesData[$index]['employee_sim'] > 0) ): ?>
                                                <td>
                                                    <?php foreach($employeesData[$index]['employee_sim'] as $key => $item): ?>
                                                        <?php echo '(' .($key + 1). ') Jenis SIM: ' . $item->jenis . ' No: ' . $item->nomorsim . '<br />'; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php if( array_key_exists('mobile', $requestData) && $requestData['mobile'] == "1" ): ?>
                                            <?php if( count($employeesData[$index]['employee_mobile'] > 0) ): ?>
                                                <td>
                                                    <?php foreach($employeesData[$index]['employee_mobile'] as $key => $item): ?>
                                                        <?php if($item->nomor != ""): ?>
                                                            <?php echo '(' .($key + 1). ') Phone: ' . $item->nomor . '<br />'; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php $no++; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class='text-right'>
                        <a class='btn btn-warning' href="{{ url('/perusahaan') }}">Kembali</a>
                        <a class='btn btn-success' href="{{ url('/perusahaan/download_pdf?' . $sessionQuery) }}">Download PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection